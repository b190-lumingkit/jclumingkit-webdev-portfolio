import './App.css';

import LandingPage from './pages/LandingPage';

function App() {
  return (
    <main className="container mx-auto h-screen">
      <LandingPage />
    </main>
  );
}

export default App;
