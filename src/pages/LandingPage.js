export default function LandingPage() {
    return(
    <>
        <header className="p-2 text-center">
            <button>Let's Connect!</button>
        </header>
        <section 
            className="
                    h-2/4 
                    grid 
                    grid-cols-1 
                    content-center 
                    justify-items-center
            ">
            <p className="text-2xl md:text-4xl">Juan Carlos Lumingkit</p>
            <p className="text-sm md:text-xl">
                Designer, Full Stack Web Developer
            </p>
            <ul className="
                    md:w-1/3
                    flex 
                    justify-around 
                    text-sm 
                    mt-5 
            ">
                <li>MongoDB</li>
                <li>ExpresJS</li>
                <li>ReactJS</li>
                <li>NodeJS</li>
            </ul>
        </section>
        <section className="p-2">
            <p className="text-md font-bold">Overview</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec ultricies urna. Quisque id lorem et metus mattis tempor. Vestibulum sed sem in arcu sodales consequat pharetra ac leo. Duis laoreet, ante ac laoreet luctus, leo nibh finibus leo, at dapibus dui nisl id elit.</p>
        </section>
        <section className="p-2">
            <p className="text-md font-bold">Projects</p>
            <section 
                className="
                        grid 
                        grid-cols-1 
                        lg:grid-cols-3 
                        grid-rows-1
                ">
                <section className="border rounded p-2 m-2">
                    <p>Personal Portfolio</p>
                    <p>Tech used: HTML5, CSS3, BOOTSTRAP v4.6</p>
                    <p>Live Demo</p>
                </section>
                <section className="border rounded p-2 m-2">
                    <p>Backend API</p>
                    <p>Tech used: NodeJS, ExpressJs, Mongoose</p>
                    <p>Live Demo</p>
                </section>
                <section className="border rounded p-2 m-2">
                    <p>E-Commerce Web App</p>
                    <p>Tech used: ReactJS</p>
                    <p>Live Demo</p>
                </section>
            </section>
        </section>
        <footer className="text-center">
            <p>Made with React and Tailwind</p>
            <p>Source code here</p>
        </footer>
    </>
    )
}